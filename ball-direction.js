;
var Game = Game || {};


Game.BallDirection = {


	collision: function(ball, element)
	{
		var x1 = ball.offset().left;
		var y1 = ball.offset().top;
		var h1 = ball.outerHeight(true);
		var w1 = ball.outerWidth(true);
		var b1 = y1 + h1;
		var r1 = x1 + w1;
		var x2 = element.offset().left;
		var y2 = element.offset().top;
		var h2 = element.outerHeight(true);
		var w2 = element.outerWidth(true);
		var b2 = y2 + h2;
		var r2 = x2 + w2;

		if (b1 < y2 || y1 > b2 || r1 < x2 || x1 > r2) {
			return {
				status:false
			};
		}

		return this.setDirection(x1, x2, w2);
	},

	setDirection: function(x1, x2, w2){


		var left = x1-x2;
		var playerWidth = w2;

		var posPercent = left * 100 / playerWidth;

		var horisontalDirection;

		if( posPercent < 0){
			horisontalDirection = 'LEFT';
		}


		if(0 < posPercent && posPercent < 20){
			horisontalDirection = 'LEFT';
		}


		if(20 < posPercent && posPercent < 60){
			horisontalDirection = 'FORWARD';
		}

		if(60 < posPercent && posPercent < 80){
			horisontalDirection = 'RIGHT';
		}

		if(posPercent > 80){
			horisontalDirection = 'RIGHT';
		}

		return {
			status:true,
			vertical:'UP',
			horisontal:horisontalDirection
		}
	},

	getDirection: function(ball, direction)
	{
		var position = ball.offset();

		if(position.top < 0){
			direction.vertical = 'DOWN';
			Game.Result.setGoal();
		}

		if(position.top > $(window).height() - 30){
			direction.vertical = 'UP';
			Game.Result.setFail();
		}

		if(position.left < 0){
			direction.horisontal = 'RIGHT';
		}

		if(position.left > $(window).width()){
			direction.horisontal = 'LEFT';
		}

		var collision = Game.BallDirection.collision( ball, $('.player') );

		if( collision.status ){ console.log(ball);
			direction.vertical = collision.vertical;
			direction.horisontal = collision.horisontal;
		};

		return direction;
	}

}