Game.Player = function()
{
	$('body').on( "keydown", $.proxy(this.move, this));
	$('body').mousemove(this.mouseMove);
}

Game.Player.prototype = {

	moveKey: {
		38:'UP',
		40:'DOWN',
		39:'RIGHT',
		37:'LEFT'
	},

	movement: {
		horisontal: 80,
		vertical:30
	},

	player: {
		element: '.player'
	},

	move: function(e)
	{
		var code = e.keyCode || e.which;
		new Game.Move({vertical:this.moveKey[code], horisontal:this.moveKey[code]}, this.movement, this.player.element);
	},

	mouseMove:function(e)
	{
        var x = e.pageX - this.offsetLeft;
        var y = e.pageY - this.offsetTop;

		$('.player').offset({top:y, left: x})
	}
}