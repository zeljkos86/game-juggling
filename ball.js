Game.Ball = function(ball, settings){
	this.init(ball, settings);
}

Game.Ball.prototype = {

	interval: 10,

	init: function(ball,settings){
		ball.data( new Game.BallSettings(settings) );
		this.run(ball);
	},

	run: function(ball)
	{
		setInterval($.proxy(this.move, this, ball),this.interval);
	},

	move: function(ball)
	{
		var direction = Game.BallDirection.getDirection(ball, ball.data().direction);
		new Game.Move(direction, ball.data().movement, ball );
	}

}