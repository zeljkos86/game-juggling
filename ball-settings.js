;
var Game = Game || {};


Game.BallSettings = function(settings){

	return this.getSettings(settings);
}

Game.BallSettings.prototype = {

	settings:{
		movement: {
			horisontal: 5,
			vertical:5
		},

		direction:{
			vertical:'DOWN',
			horisontal:'LEFT'
		}
	},

	getSettings:function(settings){

		this.settings.direction = this.setDirection(settings);
		this.settings.movement = this.setMovement(settings);

		return this.settings;
	},

	setMovement:function(settings){ debugger;
		return settings.movment == undefined ?
			this.settings.movement :
			settings.movement;
	},

	setDirection: function(settings){
		return settings.direction == undefined ?
			this.settings.direction :
			settings.direction;
	}
}