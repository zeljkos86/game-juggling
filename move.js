Game.Move = function(move, movement, element){

	var position = $(element).offset();

	if(move.vertical == 'UP') {
		position.top -= movement.vertical;
	}

	if(move.vertical == 'DOWN') {
		position.top += movement.vertical;
	}

	if(move.horisontal == 'RIGHT') {
		position.left += movement.horisontal;
	}

	if(move.horisontal == 'LEFT') {
		position.left -= movement.horisontal;
	}

	if(move.horisontal == 'FORWARD') {
		position.left = position.left ;
	}

	$(element).offset({top: position.top, left : position.left});
}